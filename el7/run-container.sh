#!/bin/bash

docker run -i \
  --volume="${PWD}:/mnt/shared:rw" \
  -e HOSTUID=$(id -u) -e HOSTGID=$(id -g) \
  -t mesa-el7-build 
