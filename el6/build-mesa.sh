#!/bin/bash

export PATH=/opt/llvm-3.8.0/bin:$PATH
export PYTHONPATH=/opt/Mako-1.0.4:$PYTHONPATH

cd /opt
git clone git://anongit.freedesktop.org/git/mesa/mesa mesa-source
cd mesa-source
VER=$(cat VERSION)-$(git log --pretty=format:"%h %ci" | head -1 | sed 's|-||g' | awk '{print $2"-"$1}')-el6

NOCONFIGURE=1 ./autogen.sh

##########
# Option summary:
##########

# Enable GL APIs
#  --enable-opengl --disable-gles1 --disable-gles2

# Disable extra state trackers that we don't care about
# --disable-va --disable-gbm --disable-xvmc --disable-vdpau

# Turn on GLdispatch
# --enable-shared-glapi

# Set up desired library features
# --disable-texture-float

# Turn off DRI (we're not using any of it)
# --disable-dri --with-dri-drivers=

# Turn on the Gallium infrastructure
# --enable-gallium-llvm

# Use LLVM's shared libraries so we don't bloat the binaries too much
# --enable-llvm-shared-libs

# Turn on only software rasterizers
# --with-gallium-drivers=swrast,swr

# Turn off EGL
# --disable-egl --disable-gbm  --with-egl-platforms=

# Turn on Gallium based OSMesa
# --enable-gallium-osmesa

# Turn on GLX (auto-determined backend)
# --enable-glx

./configure \
  --enable-opengl --disable-gles1 --disable-gles2           \
  --disable-va --disable-gbm --disable-xvmc --disable-vdpau \
  --enable-shared-glapi                                     \
  --disable-texture-float                                   \
  --disable-dri --with-dri-drivers=                         \
  --enable-gallium-llvm --enable-llvm-shared-libs           \
  --with-gallium-drivers=swrast,swr                         \
  --disable-egl --disable-gbm --with-egl-platforms=         \
  --enable-gallium-osmesa                                   \
  --enable-glx                                              \
  --prefix=/opt/mesa-${VER}
 
make -j$(grep -c "^processor" /proc/cpuinfo)
make install
cd /opt

# Before packaging up the install, get rid of the location-specific files
rm -rf /opt/mesa-${VER}/lib/pkgconfig /opt/mesa-${VER}/lib/*.la

# Copy in the nexessary LLVM dependencies
cp -P /opt/llvm-3.8.0/lib/libLLVM*.so* /opt/mesa-${VER}/lib
cp -P /opt/llvm-3.8.0/lib/libLTO*.so* /opt/mesa-${VER}/lib

# Make the archive
tar -cJvf mesa-${VER}.tar.xz mesa-${VER}

cp mesa-${VER}.tar.xz /mnt/shared
if [ -n "${HOSTUID}" ]
then
  chown ${HOSTUID} /mnt/shared/mesa-${VER}.tar.xz
fi
if [ -n "${HOSTGID}" ]
then
  chgrp ${HOSTGID} /mnt/shared/mesa-${VER}.tar.xz
fi

