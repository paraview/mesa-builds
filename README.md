# mesa-builds
Building various configurations of Mesa3D

This project builds a collection of binaries for Mesa3D with various software renderers to be packaged with ParaView binaries. This also shows the configuration flags to use for building each of the renderers.

To use this, first setup the build environment with build-container.sh.  Then executing the run-container.sh script will clone the latest Mesa GIT repository, build it with LLVMPipe and OpenSWR, and copy the resulting tarball into your current directory.
